package com.suko.shrimp.activity.report;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import java.util.ArrayList;

import com.github.mikephil.charting.utils.ColorTemplate;
import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

public class ReportDetailActivity extends AppCompatActivity  {

    private Toolbar mToolbar;
    DBHelperApps dbHelperApps;
    protected Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_detail);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Laporan "+getIntent().getStringExtra("nama"));
        dbHelperApps = new DBHelperApps(this);
        BarChart barChart = (BarChart) findViewById(R.id.chart);

        SQLiteDatabase db = dbHelperApps.getReadableDatabase();
        cursor = db.rawQuery("SELECT nama, (harga_panen*berat_total) AS panen,modal_benur from penjadwalan where idtambak='"+getIntent().getStringExtra("id")+"'",null);
        cursor.moveToFirst();
        ArrayList<String> labels = new ArrayList<String>();
        ArrayList<BarEntry> panenn = new ArrayList<>();
        ArrayList<BarEntry> modal = new ArrayList<>();
        for (int i = 0;i<cursor.getCount();i++)
        {
            cursor.moveToPosition(i);
            labels.add(cursor.getString(0).toString());
            panenn.add(new BarEntry(Float.parseFloat(cursor.getString(1).toString()),i));
            modal.add(new BarEntry(Float.parseFloat(cursor.getString(2).toString()),i));
        }

        //labels.add("P2");
        //labels.add("P3");
        //labels.add("P4");
        //labels.add("P5");
        //labels.add("P6");

        // for create Grouped Bar chart
        //ArrayList<BarEntry> group1 = new ArrayList<>();
        /*group1.add(new BarEntry(4f, 0));
        group1.add(new BarEntry(8f, 1));
        group1.add(new BarEntry(6f, 2));
        group1.add(new BarEntry(12f, 3));
        group1.add(new BarEntry(18f, 4));
        group1.add(new BarEntry(9f, 5));

        ArrayList<BarEntry> group2 = new ArrayList<>();
        group2.add(new BarEntry(6f, 0));
        group2.add(new BarEntry(7f, 1));
        group2.add(new BarEntry(8f, 2));
        group2.add(new BarEntry(12f, 3));
        group2.add(new BarEntry(15f, 4));
        group2.add(new BarEntry(10f, 5));*/

        BarDataSet barDataSet1 = new BarDataSet(modal, "Modal");
        barDataSet1.setColor(Color.BLUE);
        // barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);

        BarDataSet barDataSet2 = new BarDataSet(panenn, "Panen");
        barDataSet2.setColor(Color.GREEN);
        //barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);

        ArrayList<BarDataSet> dataset = new ArrayList<>();
        dataset.add(barDataSet1);
        dataset.add(barDataSet2);


        BarData data = new BarData(labels, dataset);
        // dataset.setColors(ColorTemplate.COLORFUL_COLORS); //
        barChart.setData(data);
        barChart.animateY(5000);
        barChart.setExtraTopOffset(10);
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
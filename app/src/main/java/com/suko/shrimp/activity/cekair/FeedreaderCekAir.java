package com.suko.shrimp.activity.cekair;

import android.provider.BaseColumns;

/**
 * Created by lenovo on 12/18/2016.
 */

public final class FeedreaderCekAir {
    private FeedreaderCekAir() {

    }
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "penjadwalan";
        public static final String COLOUMN_IDTAMBAK = "idtambak";
        public static final String COLOUMN_TGLCEK = "tgl_cek";
        public static final String COLOUMN_PARAMETER = "parameter";
        public static final String COLOUMN_HASIL = "hasil";
    }
}

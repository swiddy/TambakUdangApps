package com.suko.shrimp.activity.cekair;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

/**
 * Created by lenovo on 12/6/2016.
 */

public class RiwayatCekAir extends AppCompatActivity {
    DBHelperApps dbHelperApps;
    private Toolbar mToolbar;
    public static RiwayatCekAir ma;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cekair_riwayat);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Riwayat Cek Air");
        dbHelperApps = new DBHelperApps(this);
        ma = this;
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

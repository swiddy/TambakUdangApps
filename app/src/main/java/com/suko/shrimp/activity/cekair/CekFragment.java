package com.suko.shrimp.activity.cekair;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

/**
 * Created by Ravi on 29/07/15.
 */
public class CekFragment extends Fragment {
    float salinitas,ketinggian;
    protected Cursor cursor;
    CheckBox cbKetinggian,cbSalinitas, cbPH, cbPDO;
    EditText etKetinggian, etSalinitas, etPH, etPDO;
    Button btnPeriksa;
    TextView tvJudulCekAir;
    DBHelperApps dbHelperApps;
    float luas;
    final int salinitaslaut = 30;

    public CekFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cekair, container, false);


        setHasOptionsMenu(true);

        tvJudulCekAir = (TextView)rootView.findViewById(R.id.tvJudulCekAir);
        cbKetinggian = (CheckBox)rootView.findViewById(R.id.cbKetinggian);
        cbSalinitas = (CheckBox)rootView.findViewById(R.id.cbSalinitas);
        cbPDO = (CheckBox)rootView.findViewById(R.id.cbPDO);
        cbPH = (CheckBox)rootView.findViewById(R.id.cbPH);
        etKetinggian = (EditText)rootView.findViewById(R.id.etKetinggian);
        etSalinitas = (EditText)rootView.findViewById(R.id.etSalinitas);
        etPDO = (EditText)rootView.findViewById(R.id.etPDO);
        etPH = (EditText)rootView.findViewById(R.id.etPH);
        btnPeriksa = (Button) rootView.findViewById(R.id.btnPeriksa);
        dbHelperApps = new DBHelperApps(getContext());
        SQLiteDatabase dbb = dbHelperApps.getReadableDatabase();
        cursor = dbb.rawQuery(
                "SELECT * FROM profil WHERE nama = '" +
                        getActivity().getIntent().getStringExtra("nama") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            luas = Float.parseFloat(cursor.getString(0).toString());

        }

        cbKetinggian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cbKetinggian.isChecked()){
                    etKetinggian.setEnabled(true);
                }else {
                    etKetinggian.setEnabled(false);
                }
            }
        });

        cbPH.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cbPH.isChecked()){
                    etPH.setEnabled(true);
                }else {
                    etPH.setEnabled(false);
                }
            }
        });

        cbPDO.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cbPDO.isChecked()){
                    etPDO.setEnabled(true);
                }else {
                    etPDO.setEnabled(false);
                }
            }
        });

        cbSalinitas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cbSalinitas.isChecked()){
                    etSalinitas.setEnabled(true);
                }else {
                    etSalinitas.setEnabled(false);
                }
            }
        });



        btnPeriksa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(getActivity(),CekAirResult.class);
                if (etKetinggian.getText().toString()!="")
                {
                    intent.putExtra("ketinggian",etKetinggian.getText().toString());
                } if (etPDO.getText().toString()!="")
                {
                    intent.putExtra("pdo",etPDO.getText().toString());
                } if (etPH.getText().toString()!="")
                {
                    intent.putExtra("ph",etPH.getText().toString());
                } if (etSalinitas.getText().toString()!="")
                {
                    intent.putExtra("salinitas",etSalinitas.getText().toString());
                }
                intent.putExtra("nama",getActivity().getIntent().getStringExtra("nama"));
                startActivity(intent);
            }

        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_cek,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_list){
            Intent intent = new Intent(getActivity(),RiwayatCekAir.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

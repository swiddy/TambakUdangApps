package com.suko.shrimp.activity.notifikasi;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by lenovo on 12/6/2016.
 */

public class BootNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            if (isMyServiceRunning(ServiceAlarmPakan.class,context) == false) {
               AlaramPakan(context);
                Log.d("Service eksekusi Boot running","Horray");
            }
        }
    }

    public boolean isMyServiceRunning(Class<?> myClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (myClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void AlaramPakan (Context context)
    {
        try {
            Integer[] jamPakan = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,15,17,18,19,20,21,22,23,24};
            //int[] MenitPakan = {0, 0, 0};
            long[] waktu = new long[jamPakan.length];
            for (int i = 0; i < jamPakan.length; i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, jamPakan[i]);
               // calendar.set(Calendar.MINUTE, MenitPakan[i]);
               // calendar.set(Calendar.SECOND, 0);
                waktu[i] = calendar.getTimeInMillis();
                Intent intent = new Intent(context, ServiceAlarmPakan.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, i, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
                alarmManager.setRepeating(alarmManager.RTC, waktu[i], AlarmManager.INTERVAL_DAY, pendingIntent);
            }
        } catch (Exception e) {
            Log.e("Error :", e.getMessage());
        }

    }


}

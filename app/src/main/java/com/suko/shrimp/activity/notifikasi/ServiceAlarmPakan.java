package com.suko.shrimp.activity.notifikasi;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.suko.shrimp.R.color.colorPrimary;

/**
 * Created by lenovo on 12/25/2016.
 */

public class ServiceAlarmPakan extends Service {
    Cursor cursor;
    DBHelperApps dbHelperApps;
    String[] nama;
    String[] id;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        AlarmPakan();
        return START_STICKY;
    }

    private void AlarmPakan(){

        Date now = new Date();
        String tanggal = new SimpleDateFormat("dd:HH:mm:ss").format(now);

        dbHelperApps = new DBHelperApps(getBaseContext());
        SQLiteDatabase db = dbHelperApps.getReadableDatabase();
        cursor = db.rawQuery("SELECT a.idtambak,b.nama FROM penjadwalan a join profil b on a.idtambak = b.no ",null);
        int idnotif = createID();
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        nama = new String[cursor.getCount()];
        id = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i=0;i<cursor.getCount();i++) {
            cursor.moveToPosition(i);
            nama[i] = cursor.getString(1).toString();
            id[i] = cursor.getString(0).toString();
            NotificationManager notificationManager = (NotificationManager)getApplicationContext().getSystemService(getBaseContext().NOTIFICATION_SERVICE);
            Intent RepeatingIntent = new Intent(getBaseContext(), PakanBerulang.class);
            RepeatingIntent.putExtra("id",id[i]);
            RepeatingIntent.putExtra("nama",nama[i]);
            RepeatingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent PendingIntent = android.app.PendingIntent.getActivity(this,100,RepeatingIntent, android.app.PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                    .setContentIntent(PendingIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(nama[i]+" waktunya kasih makan")
                    .setContentText("Saran pakan 5 kg")
                    .setColor(getResources().getColor(R.color.colorPrimary))
                    .setSound(alarmSound)
                    .setAutoCancel(true);
            notificationManager.notify(idnotif+i, mBuilder.build());
        }
    }

    public int createID(){
        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));
        return id;
    }
}

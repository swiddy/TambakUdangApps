package com.suko.shrimp.activity.profile;
/**
 * Created by FATHUR (Okedroid.com) on 3/28/2016.
 */

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

public class LihatProfile extends AppCompatActivity {
    protected Cursor cursor;
    DBHelperApps dbHelper;
    Button ton2;
    TextView tvLuas, tvNama,tvPemilik;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_profile);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DBHelperApps(this);
        tvLuas = (TextView) findViewById(R.id.tvNama);
        tvNama = (TextView) findViewById(R.id.tvLuas);
        tvPemilik = (TextView) findViewById(R.id.tvPemilik);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM profil WHERE no = '" +
                getIntent().getStringExtra("idtambak") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            tvLuas.setText(cursor.getString(1).toString());
            tvPemilik.setText(cursor.getString(2).toString());
            tvNama.setText(cursor.getString(3).toString());
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
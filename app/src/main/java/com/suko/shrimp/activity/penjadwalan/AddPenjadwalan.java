package com.suko.shrimp.activity.penjadwalan;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

import java.util.Calendar;

public class AddPenjadwalan extends AppCompatActivity {
    TextView tvJadwalbaru;
    Button btnSimpanJdwlBaru;
    EditText etTglMulai,etNama,etJumlahBenur,etUkuranBenur,ethargaBenur,etHargaPakan,etHargaPupuk;
    DBHelperApps jadwal;

    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;

    private Toolbar mToolbar;


    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, mYear, mMonth, mDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        etTglMulai.setText(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.penjadwalan_baru);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        jadwal = new DBHelperApps(this);

        etTglMulai = (EditText)findViewById(R.id.etTglMulai);
        etNama = (EditText)findViewById(R.id.etNamaPenjadwalan);
        ethargaBenur = (EditText)findViewById(R.id.etHargaBenur);
        etJumlahBenur = (EditText)findViewById(R.id.etJumlahBenur);
        etUkuranBenur = (EditText)findViewById(R.id.etUkuranBenur);
        tvJadwalbaru = (TextView) findViewById(R.id.tvJadwalBaru);
        etHargaPakan = (EditText) findViewById(R.id.etHargaPakan);
        etHargaPupuk = (EditText) findViewById(R.id.etHargaPupuk);
        btnSimpanJdwlBaru = (Button)findViewById(R.id.btnSimpanJdwlBaru);
        tvJadwalbaru.setText("Penjadwalan Baru "+getIntent().getStringExtra("nama"));
        btnSimpanJdwlBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SQLiteDatabase db = jadwal.getWritableDatabase();
                String idtambak = getIntent().getStringExtra("idtambak");
                String namapenjadwalan = etNama.getText().toString();
                String tgl = etTglMulai.getText().toString();
                Integer hargaBenur = Integer.parseInt(ethargaBenur.getText().toString());
                Integer jumlahBenur = Integer.parseInt(etJumlahBenur.getText().toString());
                Integer ukuranbenur = Integer.parseInt(etUkuranBenur.getText().toString());
                Integer hargapakan = Integer.parseInt(etHargaPakan.getText().toString());
                Integer hargapupuk = Integer.parseInt(etHargaPupuk.getText().toString());
                try {
                    ContentValues values = new ContentValues();
                    values.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_IDTAMBAK,idtambak);
                    values.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_NAMA, namapenjadwalan);
                    values.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_TGL_MULAI, tgl);
                    values.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_MODAL_BENUR, hargaBenur);
                    values.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_JUMLAH_BENUR, jumlahBenur);
                    values.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_UKURAN_BENUR, ukuranbenur);
                    values.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_HARGA_PAKAN, hargapakan);
                    values.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_HARGA_PUPUK, hargapupuk);
                    // Insert the new row, returning the primary key value of the new row
                    long newRowId = db.insert(FeedReaderPenjadwalan.FeedEntry.TABLE_NAME, null, values);
                    if(newRowId >0) {
                        Intent intent = new Intent(AddPenjadwalan.this,PenjadwalanActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        finish();
                    }else{
                       Toast.makeText(getApplicationContext(),"Gagal Disimpan", Toast.LENGTH_LONG).show();
                    }

                }catch (SQLiteException e){
                    Log.e("error",e.getMessage());
                }


            }
        });


        etTglMulai.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                   showDialog(999);
                }
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
package com.suko.shrimp.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.suko.shrimp.activity.notifikasi.NotificationReceiver;
import com.suko.shrimp.activity.notifikasi.ServiceAlarmPakan;
import com.suko.shrimp.activity.profile.ProfileActivity;

import java.util.Calendar;

/**
 * Created by lenovo on 7/7/2017.
 */

public class MainActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        if (isMyServiceRunning(ServiceAlarmPakan.class)==false)
        {
            AlaramPakan();
        }
    }
    public boolean isMyServiceRunning(Class<?> myClass) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (myClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void AlaramPakan ()
    {
        try {
            Integer[] jamPakan = {5};
            int[] MenitPakan = {30};
            long[] waktu = new long[jamPakan.length];
            for (int i = 0; i < jamPakan.length; i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, jamPakan[i]);
                calendar.set(Calendar.MINUTE, MenitPakan[i]);
                calendar.set(Calendar.SECOND, 0);
                waktu[i] = calendar.getTimeInMillis();
                Intent intent = new Intent(this, NotificationReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, i, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.setRepeating(alarmManager.RTC, waktu[i], AlarmManager.INTERVAL_DAY, pendingIntent);
            }
        } catch (Exception e) {
            Log.e("Error :", e.getMessage());
        }

    }
}

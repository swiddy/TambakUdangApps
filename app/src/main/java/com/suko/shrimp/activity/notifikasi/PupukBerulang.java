package com.suko.shrimp.activity.notifikasi;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by lenovo on 12/6/2016.
 */

public class PupukBerulang extends AppCompatActivity {
    Button btnSimpan;
    EditText etPupuk;
    DBHelperApps dbHelperApps;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pupukrepeat);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etPupuk = (EditText)findViewById(R.id.etPupuk);
        btnSimpan = (Button)findViewById(R.id.btnSimpanPupuk);
        dbHelperApps = new DBHelperApps(this);

        if (getIntent().getStringExtra("nama")==null)
        {
            getSupportActionBar().setTitle("Kosong");
        }else {
            getSupportActionBar().setTitle("Form pupuk"+getIntent().getStringExtra("nama"));
        }


        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pupuk = etPupuk.getText().toString();
                SQLiteDatabase db = dbHelperApps.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(FeedReaderNotif.FeedEntry.COLOUMN_BERAT_PUPUK,pupuk);
                values.put(FeedReaderNotif.FeedEntry.COLOUMN_IDTAMBAK_PUPUK,getIntent().getStringExtra("id"));
                values.put(FeedReaderNotif.FeedEntry.COLOUMN_TGL_PUPUK,getDateTime());
                try {
                    long res = db.insert(FeedReaderNotif.FeedEntry.TABLE_NAME_PUPUK, null, values);
                    if (res>0)
                    {
                        finish();
                    }else
                    {
                        Toast.makeText(getApplicationContext(),"Gagal disimopan",Toast.LENGTH_LONG).show();
                    }
                }catch (SQLiteException e){
                    Log.e("Error :", e.getMessage());
                }

            }
        });
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

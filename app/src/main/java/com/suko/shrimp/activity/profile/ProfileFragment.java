package com.suko.shrimp.activity.profile;

/**
 * Created by Ravi on 29/07/15.
 */
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;
import com.suko.shrimp.activity.penjadwalan.AddPenjadwalan;
import com.suko.shrimp.activity.penjadwalan.PenjadwalanActivity;


public class ProfileFragment extends Fragment {
    String[] daftar;
    String[]id;
    ListView ListView01;
    protected Cursor cursor;
    DBHelperApps dbcenter;
    FloatingActionButton BtnNew;


    public static ProfileFragment ma;
    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ma = this;
        dbcenter = new DBHelperApps(getContext());
        RefreshList(rootView);
        BtnNew = (FloatingActionButton)rootView.findViewById(R.id.btnNewProfile);

        BtnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(),AddProfile.class);
                startActivity(i);
            }
        });

        return rootView;
    }

    public void RefreshList(View view){
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM profil",null);
        daftar = new String[cursor.getCount()];
        id = new String[cursor.getCount()];
        cursor.moveToFirst();

        for (int cc = 0; cc < cursor.getCount(); cc++){
            cursor.moveToPosition(cc);
            daftar[cc] = cursor.getString(1).toString();
            id[cc] = cursor.getString(0).toString();

        }
        ListView01 = (ListView)view.findViewById(R.id.listView1);
        ListView01.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, daftar));

        ListView01.setSelected(true);
        ListView01.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                final String selection = daftar[arg2]; //.getItemAtPosition(arg2).toString();
                final String idx = id[arg2];
                final CharSequence[] dialogitem = {"Lihat", "Update", "Hapus","Penjadwalan baru","Daftar Penjadwalan","Cek "};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Pilihan");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch(item){
                            case 0 :
                                Intent i = new Intent(getActivity(), LihatProfile.class);
                                i.putExtra("nama", selection);
                                i.putExtra("idtambak", idx);
                                startActivity(i);
                                break;
                            case 1 :
                                Intent in = new Intent(getActivity(), UpdateProfile.class);
                                in.putExtra("nama", selection);
                                in.putExtra("idtambak", idx);
                                startActivity(in);
                                break;
                            case 2 :
                                try {
                                    SQLiteDatabase db = dbcenter.getWritableDatabase();
                                    db.execSQL("delete from profil where no = '"+idx+"'");
                                    Toast.makeText(getContext(),"Hapus berhasil", Toast.LENGTH_LONG);
                                }catch (SQLException e){
                                    Log.e("error :",e.getMessage());
                                }
                                Intent intent = new Intent(getActivity(),ProfileActivity.class);
                                startActivity(intent);
                                break;
                            case 3 :
                                Intent inte = new Intent(getActivity(),AddPenjadwalan.class );
                                inte.putExtra("nama",selection);
                                inte.putExtra("idtambak",idx);
                                startActivity(inte);
                                break;
                            case 4 :
                                Intent inten = new Intent(getActivity(),PenjadwalanActivity.class );
                                inten.putExtra("idtambak",idx);
                                startActivity(inten);
                                break;
                            default:

                        }
                    }
                });
                builder.create().show();
            }});
        ((ArrayAdapter)ListView01.getAdapter()).notifyDataSetInvalidated();

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

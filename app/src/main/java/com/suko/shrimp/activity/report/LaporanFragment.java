package com.suko.shrimp.activity.report;

/**
 * Created by Ravi on 29/07/15.
 */
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;


public class LaporanFragment extends Fragment {
    DBHelperApps dbHelperApps;
    protected Cursor cursor;
    ListView lvReport;

    public LaporanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_laporan, container, false);
        lvReport = (ListView)rootView.findViewById(R.id.lvReport);
        try {
            dbHelperApps = new DBHelperApps(getContext());
            SQLiteDatabase db = dbHelperApps.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM profil",null);
            cursor.moveToFirst();
            final String[] nama = new String[cursor.getCount()];
            final String[] id = new String[cursor.getCount()];
            for (int i = 0;i<cursor.getCount();i++)
            {
                cursor.moveToPosition(i);
                id[i] = cursor.getString(0).toString();
                nama[i] = cursor.getString(1).toString();
            }

            ArrayAdapter adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,nama);
            lvReport.setAdapter(adapter);
            lvReport.setSelected(true);
            lvReport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int idd, long l) {
                    Intent ii = new Intent(getContext(),ReportDetailActivity.class);
                    ii.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    final String idx = id[idd];
                    final String namax = nama[idd];
                    ii.putExtra("nama",namax);
                    ii.putExtra("id",idx);
                    startActivity(ii);
                }
            });
        }catch (SQLiteException e)
        {
            Log.e("error :",e.getMessage());
        }


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

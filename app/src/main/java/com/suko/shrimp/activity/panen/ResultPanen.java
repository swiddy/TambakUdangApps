package com.suko.shrimp.activity.panen;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

/**
 * Created by lenovo on 7/3/2017.
 */

public class ResultPanen extends AppCompatActivity {
    private Toolbar mToolbar;
    ListView listitem;
    String[] judul = {"TONASE","MBW","SR","UKURAN"};
    DBHelperApps dbHelperApps;
    protected Cursor cursor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panen_result);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listitem = (ListView)findViewById(R.id.lvitem);

        dbHelperApps = new DBHelperApps(this);//tvCoba.setText(getIntent().getStringExtra("id"));
        try {
            SQLiteDatabase db = dbHelperApps.getReadableDatabase();
            cursor = db.rawQuery("SELECT " +
                    "berat_total AS tonase, " +
                    "berat_panen/jumlah_panen AS MBW, " +
                    "(jumlah_panen/jumlah_benur)*100 AS SR, " +
                    "berat_panen/jumlah_panen AS ukuran " +
                    "FROM penjadwalan where no = '"
                    + getIntent().getStringExtra("id") + "'", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                String[] itemm = new String[4];
                for (int i = 0; i < 4; i++) {
                    itemm[i] = judul[i]+" : "+cursor.getString(i).toString();
                }
                ArrayAdapter adapter2 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,itemm);
                listitem.setAdapter(adapter2);
            }
        }catch (SQLiteException e)
        {
            Log.e("Error :",e.getMessage());
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

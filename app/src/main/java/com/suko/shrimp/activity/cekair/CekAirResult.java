package com.suko.shrimp.activity.cekair;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.suko.shrimp.R;

/**
 * Created by lenovo on 6/30/2017.
 */

public class CekAirResult extends AppCompatActivity {
    private Toolbar mToolbar;
    TextView tv1;
    Float ketinggian;
    Float ph;
    Float pdo;
    Double salinitas = 0.0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cekair_hasil);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Hasil Cek"+getIntent().getStringExtra("nama"));

        if (getIntent().getStringExtra("salinitas") != "")
        {
            salinitas = Double.parseDouble(getIntent().getStringExtra("salinitas"));
            CekSalinitas(salinitas);
        }
    }

    public void CekSalinitas(Double v_salinitas)
    {
        TextView tvv = (TextView)findViewById(R.id.tv);
        tvv.setText("Salinitas"+v_salinitas);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

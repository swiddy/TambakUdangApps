package com.suko.shrimp.activity.profile;

import android.provider.BaseColumns;

/**
 * Created by lenovo on 12/17/2016.
 */

public final class FeedReaderProfil {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderProfil() {}

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String COLOUMN_ID = "no";
        public static final String TABLE_NAME = "profil";
        public static final String COLUMN_NAMA = "nama";
        public static final String COLUMN_PEMILIK = "pemilik";
        public static final String COLUMN_LUAS = "luas";

    }
}

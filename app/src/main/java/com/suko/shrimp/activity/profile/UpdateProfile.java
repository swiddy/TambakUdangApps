package com.suko.shrimp.activity.profile;
/**
 * Created by FATHUR (Okedroid.com) on 3/28/2016.
 */

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

import static android.content.Intent.FLAG_ACTIVITY_NO_ANIMATION;


public class UpdateProfile extends AppCompatActivity {
    protected Cursor cursor;
    DBHelperApps dbHelper;
    Button btnUpdate, ton2;
    EditText etLuas,etNama,etPemilik;
    String id;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DBHelperApps(this);
        etLuas = (EditText) findViewById(R.id.etLuas);
        etNama = (EditText) findViewById(R.id.etNama);
        etPemilik = (EditText)findViewById(R.id.etPemilikedit);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM profil WHERE no = '" +
                getIntent().getStringExtra("idtambak") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            id = cursor.getString(0).toString();
            etNama.setText(cursor.getString(1).toString());
            etPemilik.setText(cursor.getString(2).toString());
            etLuas.setText(cursor.getString(3).toString());

        }
        btnUpdate = (Button) findViewById(R.id.btnUpdate);

        // daftarkan even onClick pada btnSimpan
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

              try {
                  // TODO Auto-generated method stub
                  SQLiteDatabase db = dbHelper.getWritableDatabase();
                  ContentValues values = new ContentValues();
                  values.put(FeedReaderProfil.FeedEntry.COLUMN_NAMA,etNama.getText().toString());
                  values.put(FeedReaderProfil.FeedEntry.COLUMN_PEMILIK,etPemilik.getText().toString());
                  values.put(FeedReaderProfil.FeedEntry.COLUMN_LUAS,etLuas.getText().toString());

                  long row = db.update(FeedReaderProfil.FeedEntry.TABLE_NAME,values,FeedReaderProfil.FeedEntry.COLOUMN_ID+"="+id,null);
                  if (row>0){
                      Intent intent = new Intent(UpdateProfile.this,ProfileActivity.class);
                      intent.addFlags(FLAG_ACTIVITY_NO_ANIMATION);
                      startActivity(intent);
                      finish();
                  }
                  else
                  {
                      Toast.makeText(getApplicationContext(),"Gagal update",Toast.LENGTH_LONG).show();
                  }

              }catch (SQLiteException e){
                  Log.e("Error :",e.getMessage());
              }
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
package com.suko.shrimp.activity.penjadwalan;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;
import com.suko.shrimp.activity.panen.AddPanen;

/**
 * Created by Ravi on 29/07/15.
 */
public class PenjadwalanFragment extends Fragment {

    TextView judul;
    ListView lvPenjadwalan;
    protected Cursor cursor;
    String[] daftar;
    String[] id;
    String[] tambak;
    public static PenjadwalanFragment ma;
    DBHelperApps dbHelperJadwal;

    public PenjadwalanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_penjadwalan, container, false);

        lvPenjadwalan = (ListView) rootView.findViewById(R.id.lvPenjadwalan);
        ma = (this);
        dbHelperJadwal = new DBHelperApps(getContext());
        dbHelperJadwal.getWritableDatabase();

        if (getActivity().getIntent().getStringExtra("idtambak")==null){
            Resfreshlist();
        }else {
            //android.app.ActionBar ab = getActionBar();
            // ab.setTitle(getIntent().getStringExtra("idtambak"));
            SQLiteDatabase db = dbHelperJadwal.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM penjadwalan WHERE idtambak = '"+getActivity().getIntent().getStringExtra("idtambak")+"'",null);
            int res = cursor.getCount();
            if (res>0){
                Resfreshlist2(Integer.parseInt(getActivity().getIntent().getStringExtra("idtambak")));
            }else {
                Toast.makeText(getContext(),"Belum ada penjadwalan", Toast.LENGTH_LONG).show();
            }

        }


        // Inflate the layout for this fragment
        return rootView;
    }

    public void Resfreshlist() {
        SQLiteDatabase db = dbHelperJadwal.getReadableDatabase();
        cursor = db.rawQuery("SELECT a.no,a.nama,b.nama FROM penjadwalan a join profil b on a.idtambak = b.no",null);
        daftar = new String[cursor.getCount()];
        id = new String[cursor.getCount()];
        tambak = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            daftar[cc] = cursor.getString(2)+" >> "+cursor.getString(1).toString();
            id[cc] = cursor.getString(0).toString();
            //   tambak[cc] = cursor.getString(2).toString();
        }
        lvPenjadwalan.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, daftar));
        lvPenjadwalan.setSelected(true);
        lvPenjadwalan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                // final String selection = daftar[arg2]; //.getItemAtPosition(arg2).toString();
                final String idx = id[arg2];
                final CharSequence[] dialogitem = {"hapus","panen"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Pilihan");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 0:
                                try {
                                    SQLiteDatabase db = dbHelperJadwal.getWritableDatabase();
                                    db.execSQL("DELETE FROM penjadwalan where no = '"+idx+"'");
                                }catch (SQLException e){
                                    Log.e("error ",e.getMessage());
                                }
                                Resfreshlist();
                                break;
                            case 1:
                                Intent intent = new Intent(getActivity(),AddPanen.class);
                                intent.putExtra("id",idx);
                                startActivity(intent);
                        }
                    }
                });
                builder.create().show();
            }

        });
        ((ArrayAdapter) lvPenjadwalan.getAdapter()).notifyDataSetInvalidated();
    }

    public void Resfreshlist2(int no) {
        SQLiteDatabase db = dbHelperJadwal.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM penjadwalan where idtambak = '" + no + "'", null);
        daftar = new String[cursor.getCount()];
        id = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            daftar[cc] = cursor.getString(2).toString();
            id[cc] = cursor.getString(0).toString();
        }
        lvPenjadwalan.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, daftar));
        lvPenjadwalan.setSelected(true);
        lvPenjadwalan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                final String selection = daftar[arg2]; //.getItemAtPosition(arg2).toString();
                final String idx = id[arg2];
                final CharSequence[] dialogitem = {"hapus", "panen"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Pilihan");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 0:
                                try {
                                    SQLiteDatabase db = dbHelperJadwal.getWritableDatabase();
                                    db.execSQL("DELETE FROM penjadwalan where no = '" + idx + "'");
                                } catch (SQLException e) {
                                    Log.e("error ", e.getMessage());
                                }
                                Resfreshlist();
                                break;
                            case 1:
                               Intent intent = new Intent(PenjadwalanActivity.this, AddPanen.class);
                                intent.putExtra("idtambak", idx);
                                startActivity(intent);
                                break;
                        }
                    }
                });
                builder.create().show();
            }

        });
        ((ArrayAdapter) lvPenjadwalan.getAdapter()).notifyDataSetInvalidated();
    }




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

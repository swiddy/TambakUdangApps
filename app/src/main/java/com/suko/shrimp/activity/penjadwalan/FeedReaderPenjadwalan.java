package com.suko.shrimp.activity.penjadwalan;

import android.provider.BaseColumns;

/**
 * Created by lenovo on 12/17/2016.
 */

public final class FeedReaderPenjadwalan {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderPenjadwalan() {}

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String COLUMN_ID_PENJADWALAN = "no";
        public static final String TABLE_NAME = "penjadwalan";
        public static final String COLUMN_IDTAMBAK = "idtambak";
        public static final String COLUMN_NAMA = "nama";
        public static final String COLUMN_TGL_MULAI = "tgl_mulai";
        public static final String COLUMN_TGL_PANEN = "tgl_panen";
        public static final String COLUMN_MODAL_BENUR = "modal_benur";
        public static final String COLUMN_JUMLAH_BENUR = "jumlah_benur";
        public static final String COLUMN_UKURAN_BENUR = "ukuran_benur";
        public static final String COLUMN_JUMLAH_PANEN = "jumlah_panen";
        public static final String COLUMN_BERAT_PANEN = "berat_panen";
        public static final String COLUMN_HARGA_PANEN = "harga_panen";
        public static final String COLUMN_HARGA_PUPUK = "harga_pupuk";
        public static final String COLUMN_HARGA_PAKAN = "harga_pakan";
    }
}

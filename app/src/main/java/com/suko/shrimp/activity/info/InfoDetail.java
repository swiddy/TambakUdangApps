package com.suko.shrimp.activity.info;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.suko.shrimp.R;

/**
 * Created by lenovo on 10/3/2016.
 */


public class InfoDetail extends AppCompatActivity {
    WebView view;
    String url;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        String[] urllist = {
                "file:///android_asset/pembibitan.html",
                "file:///android_asset/pemanenan.html",
                "file:///android_asset/benur.html",
                "file:///android_asset/air.html"
        };

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("judul"));
        int id = getIntent().getIntExtra("id",0);

        url = urllist[id];

        view = (WebView) this.findViewById(R.id.webview);
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
        view.setWebViewClient(new MyBrowser());


    }

   private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

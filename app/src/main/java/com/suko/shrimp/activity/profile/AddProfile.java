package com.suko.shrimp.activity.profile;


import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

public class AddProfile extends AppCompatActivity {
    protected Cursor cursor;
    DBHelperApps dbHelper;
    Button btnSimpan, ton2;
    EditText etNama,etLuas,etPemilik;

    public static AddProfile ma;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_profile);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DBHelperApps(this);

        etNama = (EditText) findViewById(R.id.etNama);
        etPemilik = (EditText) findViewById(R.id.etPemilik);
        etLuas = (EditText) findViewById(R.id.etLuas);
        btnSimpan = (Button) findViewById(R.id.btnSimpan);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
               try {

                   String pemilik = etPemilik.getText().toString();
                   String nama = etNama.getText().toString();
                   int luas = Integer.parseInt(etLuas.getText().toString());
                   SQLiteDatabase db = dbHelper.getWritableDatabase();
                   ContentValues values = new ContentValues();
                   values.put(FeedReaderProfil.FeedEntry.COLUMN_NAMA, nama);
                   values.put(FeedReaderProfil.FeedEntry.COLUMN_PEMILIK, pemilik);
                   values.put(FeedReaderProfil.FeedEntry.COLUMN_LUAS, luas);

                   // Insert the new row, returning the primary key value of the new row
                   long newRowId = db.insert(FeedReaderProfil.FeedEntry.TABLE_NAME, null, values);
                   if (newRowId>0)
                   {
                       Intent intent = new Intent(AddProfile.this,ProfileActivity.class);
                       intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                       startActivity(intent);
                       finish();
                   }else
                   {
                       Toast.makeText(getApplicationContext(),"Gagal disimpan",Toast.LENGTH_LONG).show();
                   }

               }catch (SQLException e){
                   e.printStackTrace();
               }
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;

    }
}
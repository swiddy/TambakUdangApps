package com.suko.shrimp.activity.notifikasi;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by lenovo on 12/6/2016.
 */

public class PakanBerulang extends AppCompatActivity {
    EditText etpakanBerulang;
    Button btnSimpanPakanBerulang;
    DBHelperApps dbHelperApps;
    TextView tvJudul;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pakanrepeat);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelperApps = new DBHelperApps(this);
        etpakanBerulang = (EditText)findViewById(R.id.etPakanBerulang);
        btnSimpanPakanBerulang =(Button)findViewById(R.id.btnSimpanPakanBerulang);
        if (getIntent().getStringExtra("nama")==null)
        {
            getSupportActionBar().setTitle("kosong");
        }else {
            getSupportActionBar().setTitle("Form Pakan"+getIntent().getStringExtra("nama"));
        }

        btnSimpanPakanBerulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelperApps.getWritableDatabase();
                Float pakan = Float.parseFloat(etpakanBerulang.getText().toString());
                ContentValues values = new ContentValues();
                values.put(FeedReaderNotif.FeedEntry.COLOUMN_BERAT_PAKAN,pakan);
                values.put(FeedReaderNotif.FeedEntry.COLOUMN_IDTAMBAK_PAKAN,getIntent().getStringExtra("id"));
                values.put(FeedReaderNotif.FeedEntry.COLOUMN_TGL_PAKAN,getDateTime());
                try {
                    db.insert(FeedReaderNotif.FeedEntry.TABLE_NAME_PAKAN, null, values);
                    Toast.makeText(getApplicationContext(),"Berhasil disimpan", Toast.LENGTH_LONG).show();
                    finish();
                }catch (SQLiteException e){
                    Log.e("Error :",e.getMessage());
                }

            }
        });
    }
    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

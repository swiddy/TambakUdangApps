package com.suko.shrimp.activity.penjadwalan;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;

/**
 * Created by lenovo on 7/3/2017.
 */

public class DetailPenjadwalan extends AppCompatActivity {
    private Toolbar mToolbar;
    ListView listjudul,listitem;
    String[] judul = {"no","idtambak","nama","tgl mulai","tglpanen","jumlahbenur","ukuran benur","beratpanen","berat total","hargapanen","harga pupuk","harga pakan"};
    String[] itemm;
    DBHelperApps dbHelperApps;
    TextView tvCoba;
    protected Cursor cursor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panen_result);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listitem = (ListView)findViewById(R.id.lvitem);

        dbHelperApps = new DBHelperApps(this);//tvCoba.setText(getIntent().getStringExtra("id"));
        try {
            SQLiteDatabase db = dbHelperApps.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM penjadwalan where no = '"
                    + getIntent().getStringExtra("id") + "'", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                String[] itemm = new String[12];
                for (int i = 0; i < 12; i++) {
                    itemm[i] = cursor.getString(i).toString();
                }
                ArrayAdapter adapter2 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,itemm);
                listitem.setAdapter(adapter2);
            }
        }catch (SQLiteException e)
        {
            Log.e("Error :",e.getMessage());
        }

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,judul);
        listjudul.setAdapter(adapter);

    }
}

package com.suko.shrimp.activity.info;

/**
 * Created by Ravi on 29/07/15.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.suko.shrimp.R;


public class InfoFragment extends Fragment {
    ListView lvInfo;
    String[] info = {"Bagaimana cara pembibitan udang","Cara Memanen udang","Cara Memperlakukan benur","Mengatur idealnya air"};



    public InfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_info, container, false);

        lvInfo = (ListView)rootView.findViewById(R.id.ListInfo);

        ArrayAdapter adapter = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,info);
        lvInfo.setAdapter(adapter);
        lvInfo.setSelected(true);
        lvInfo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent in = new Intent(getActivity(),InfoDetail.class);
                        in.putExtra("judul",info[i]);
                        in.putExtra("id",i);
                        startActivity(in);

            }
        });


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

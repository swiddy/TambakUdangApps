package com.suko.shrimp.activity.panen;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.suko.shrimp.DBHelperApps;
import com.suko.shrimp.R;
import com.suko.shrimp.activity.penjadwalan.FeedReaderPenjadwalan;

import java.util.Calendar;

/**
 * Created by lenovo on 12/5/2016.
 */

public class AddPanen extends AppCompatActivity {
    //@Override
    EditText etBeratPanen,etTglPanen,etBeratTotal,ethargaPanen,etJumlahPanen;
    Button btnPanen;
    DBHelperApps db;

    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;
        private Toolbar mToolbar;


    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, mYear, mMonth, mDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        etTglPanen.setText(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panen_layout);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        db = new DBHelperApps(this);

        etBeratPanen = (EditText)findViewById(R.id.etBeratPanen);
        etTglPanen = (EditText)findViewById(R.id.etTglPanen);
        ethargaPanen = (EditText)findViewById(R.id.etHargaPanen);
        etJumlahPanen = (EditText)findViewById(R.id.etJumlahPanen);
        btnPanen = (Button)findViewById(R.id.btnSimpanPanen);

        btnPanen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SQLiteDatabase db1 = db.getWritableDatabase();
                Float beratpanen = Float.parseFloat(etBeratPanen.getText().toString());
                String tglpanen = etTglPanen.getText().toString();
                int id = Integer.parseInt(getIntent().getStringExtra("id"));
                int hargapanen = Integer.parseInt(ethargaPanen.getText().toString());
                int jumlah_panen = Integer.parseInt(etJumlahPanen.getText().toString());

                try {
                    ContentValues cv = new ContentValues();
                    cv.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_TGL_PANEN,tglpanen);
                    cv.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_JUMLAH_PANEN,jumlah_panen);
                    cv.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_BERAT_PANEN,beratpanen);
                    cv.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_TGL_PANEN,tglpanen);
                    cv.put(FeedReaderPenjadwalan.FeedEntry.COLUMN_HARGA_PANEN,hargapanen);
                    long key = db1.update(FeedReaderPenjadwalan.FeedEntry.TABLE_NAME,cv,FeedReaderPenjadwalan.FeedEntry.COLUMN_ID_PENJADWALAN+"="+id,null);
                    if(key>0){
                        Intent inn = new Intent(AddPanen.this,ResultPanen.class);
                        inn.putExtra("id",getIntent().getStringExtra("id"));
                        startActivity(inn);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),"Gagal disimpan", Toast.LENGTH_LONG).show();
                    }
                }catch (SQLiteException e){
                    Log.e("Error :",e.getMessage());
                }
            }
        });

        etTglPanen.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    showDialog(999);
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

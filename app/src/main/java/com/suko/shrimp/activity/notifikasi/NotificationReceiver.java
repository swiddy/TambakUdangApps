package com.suko.shrimp.activity.notifikasi;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by lenovo on 7/10/2017.
 */

public class NotificationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Informasi","Service eksekusi");
        Intent servis = new Intent(context,ServiceAlarmPakan.class);
        context.startService(servis);
    }

}

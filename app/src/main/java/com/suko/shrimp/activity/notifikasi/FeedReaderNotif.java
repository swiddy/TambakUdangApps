package com.suko.shrimp.activity.notifikasi;

import android.provider.BaseColumns;

/**
 * Created by lenovo on 12/18/2016.
 */

public final class FeedReaderNotif {
    private FeedReaderNotif() {
    }
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME_PAKAN = "pakan";
        public static final String TABLE_NAME_PUPUK = "pupuk";
        public static final String COLOUMN_IDTAMBAK_PUPUK = "idtambak";
        public static final String COLOUMN_IDTAMBAK_PAKAN = "idtambak";
        public static final String COLOUMN_BERAT_PUPUK = "berat";
        public static final String COLOUMN_BERAT_PAKAN = "berat";
        public static final String COLOUMN_TGL_PUPUK = "tgl";
        public static final String COLOUMN_TGL_PAKAN = "tgl";


    }
}

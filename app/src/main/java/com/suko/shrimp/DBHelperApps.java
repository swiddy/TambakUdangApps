package com.suko.shrimp;

/**
 * .
 */


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelperApps extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "fishpond9.db";
    private static final int DATABASE_VERSION = 1;
    public DBHelperApps(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        try {
            String sql = "create table profil(no integer primary key autoincrement, nama text null,pemilik text null, luas integer null);";
            db.execSQL(sql);
            String sql2 = "create table penjadwalan (" +
                    "no integer primary key autoincrement," +
                    "idtambak integer not null," +
                    "nama text null," +
                    "tgl_mulai date null, " +
                    "tgl_panen date null," +
                    "jumlah_benur integer null, " +
                    "ukuran_benur integer null, " +
                    "modal_benur integer null," +
                    "berat_panen integer null," +
                    "berat_total integer null," +
                    "jumlah_panen integer null," +
                    "harga_panen integer null," +
                    "harga_pupuk integer null," +
                    "harga_pakan integer null);";

            Log.d("Data", "onCreate: " + sql2);
            db.execSQL(sql2);
            String sql3 = "CREATE TABLE pupuk (" +
                    "id integer primary key autoincrement," +
                    "idtambak integer not null," +
                    "tgl DATETIME not null," +
                    "berat integer null)";
            db.execSQL(sql3);
            Log.d("Data", "onCreate: " + sql3);
            String sql4 = "CREATE TABLE pakan (" +
                    "id integer primary key autoincrement," +
                    "idtambak integer not null," +
                    "tgl DATETIME not null," +
                    "berat integer null)";
            db.execSQL(sql4);
            Log.d("Data", "onCreate: " + sql4);
        }catch (SQLiteException e){
            Log.e("error :",e.getMessage());
        }
      //  db.close();

    }

    public Cursor fetchAllProfiles(SQLiteDatabase db) {

        return db.query("profile", new String[] { "no", "nama","luas" }, null, null,

        null, null, null);

    }



    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub

    }

}
